package com.sya.presupo.adaptadores;

import java.util.ArrayList;

import com.sya.presupo.estructuras.eItems;
import com.sya.presupo.model.Item;
import com.sya.presupo.util.MyMath;

import scz.bo.spresupo.R;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;



public class ListaPresuAdaptador extends BaseAdapter {
	public ArrayList<eItems> list;
	Activity activity;
	public ListaPresuAdaptador(Activity activity, ArrayList<eItems> lista) {
		super();
		this.activity =activity;
		this.list = lista;
		// TODO Auto-generated constructor stub
	}
	public ArrayList<eItems> getList() {
		return list;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return list.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int arg0, View convertView, ViewGroup arg2) {
		// TODO Auto-generated method stub
		Campos campos;
		LayoutInflater inflater =  activity.getLayoutInflater();

		if (convertView == null)
		{
			convertView = inflater.inflate(R.layout.listarealizarpre, null);
			campos = new Campos();
			campos.tvDescripcion = (TextView) convertView.findViewById(R.id.tvPDescripcion);
			campos.tvPrecio = (TextView) convertView.findViewById(R.id.tvPPrecio);
			campos.tvCantidad =(TextView) convertView.findViewById(R.id.tvPCantidad);
			campos.tvTotal =(TextView) convertView.findViewById(R.id.tvpTotal);
			
			convertView.setTag(campos);
		}
		else
		{
			campos = (Campos) convertView.getTag();
		}

		eItems map = list.get(arg0);
		 
		campos.tvDescripcion.setText(map.getDescripcion());
		campos.tvPrecio.setText(Double.toString(map.getPrecio()));
		campos.tvCantidad.setText(Double.toString(map.getCantidad()));
		double total =MyMath.Redondear( map.getCantidad()*map.getPrecio());
		campos.tvTotal.setText(Double.toString(total));

		return convertView;
	}
	static class Campos {
	       TextView tvDescripcion;
	       TextView tvPrecio;
	       TextView tvCantidad;
	       TextView tvTotal;
	  }

}

