package com.sya.presupo.adaptadores;



import java.util.ArrayList;
import java.util.HashMap;

import com.sya.presupo.estructuras.eItems;

import scz.bo.spresupo.R;



import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public class listaProductosAdaptador extends BaseAdapter{
	public ArrayList<eItems> list;
	Activity activity;
	public listaProductosAdaptador(Activity activity, ArrayList<eItems> list) {
		// TODO Auto-generated constructor stub
		super();
		this.activity = activity;
		this.list = list;
	}
	public ArrayList<eItems> getList() {
		return list;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return list.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		// TODO Auto-generated method stub
		Campos campos;
		LayoutInflater inflater =  activity.getLayoutInflater();

		if (convertView == null)
		{
			convertView = inflater.inflate(R.layout.listaproductos, null);
			campos = new Campos();
			campos.tvDescripcion = (TextView) convertView.findViewById(R.id.tvDescripcion);
			campos.tvUmedida = (TextView) convertView.findViewById(R.id.tvUmedida);
			campos.tvPrecio = (TextView) convertView.findViewById(R.id.tvPrecio);
			campos.chkEstado = (CheckBox) convertView.findViewById(R.id.chbestado);
			
			convertView.setTag(campos);
		}
		else
		{
			campos = (Campos) convertView.getTag();
		}

		eItems map = list.get(position);
		 
		campos.tvDescripcion.setText(map.getDescripcion());
		campos.tvUmedida.setText(map.getUmedida());
		campos.tvPrecio.setText(Double.toString(map.getPrecio()));
		campos.chkEstado.setChecked(map.isChecable());
		

	return convertView;
	}
	static class Campos {
	       TextView tvDescripcion;
	       TextView tvUmedida;
	       TextView tvPrecio;
	       CheckBox chkEstado;
	  }
}
