package com.sya.presupo.datos;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sya.presupo.util.UtilDB;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class ManangerSQlite extends MyDataSource {
	public ManangerSQlite(Context contex) {
		super(contex);
		// TODO Auto-generated constructor stub
	}

	public boolean guardar(Object obj) {
		
	
		try {
			open();
			String consulta = UtilDB.getConsultaInsertarID(obj);
			db.execSQL(consulta);
			Log.d("CONSULTA", consulta);
			close();
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}

	}
	

	public Object getObjectCampo(Object obj, String campo, Object data) {

		try {
			open();
			String consulta = UtilDB.getObjectCampo(obj, campo, data);

			Cursor c = db.rawQuery(consulta, null);
			if (c.moveToFirst()) {

				do {
					obj = getData(c, obj);

				} while (c.moveToNext());
			}
			c.close();
			close();
			return obj;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	public boolean guardarID(Object obj) {

		try {
			open();
			
			db.execSQL(UtilDB.getConsultaInsertarID(obj));
			close();
			return true;
		} catch (Exception ex) {   
			Logger.getLogger(ManangerSQlite.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		return false;
	}
	public boolean eliminarTabla(Object obj) {

		try {
			open();
			db.execSQL(UtilDB.getConsultaEliminarTodos(obj));
			close();
			return true;
		} catch (Exception ex) {
			Logger.getLogger(ManangerSQlite.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		return false;
	}

	public ArrayList<Object> getLista(Object obj) {
		ArrayList<Object> lista = new ArrayList<Object>();

		openRead();
		String consulta = UtilDB.getConsultaTodos(obj);
		Cursor c = db.rawQuery(consulta, null);
       
		
		if (c.moveToFirst()) {

			do {
				obj = getData(c, obj);
				lista.add(obj);
			} while (c.moveToNext());
		}
		c.close();
		close();
		return lista;

	}
	public ArrayList<Object> getListaCampo(Object obj, String campo, Object data) {
		ArrayList<Object> lista = new ArrayList<Object>();

		open();
		String consulta =  UtilDB.getObjectCampo(obj, campo, data);
		Cursor c = db.rawQuery(consulta, null);

		
		if (c.moveToFirst()) {

			do {
				obj = getData(c, obj);
				lista.add(obj);
			} while (c.moveToNext());
		}
		c.close();
		close();
		return lista;

	}
	

	public Object getUltimoObject(Object obj) throws SQLException {
		try {
			open();
			String consulta = UtilDB.getConsultaUltimo(obj);

			Cursor c = db.rawQuery(consulta, null);
			if (c.moveToFirst()) {

				do {
					obj = getData(c, obj);

				} while (c.moveToNext());
			}
			c.close();
			return obj;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}

	}

	public Object getObjectId(Object obj) {

		try {
			open();
			String consulta = UtilDB.getObjectoID(obj);

			Cursor c = db.rawQuery(consulta, null);
			if (c.moveToFirst()) {

				do {
					obj = getData(c, obj);

				} while (c.moveToNext());
			}
			c.close();
			c.close();
			return obj;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}

	}

	private Object getData(Cursor rs, Object obj) {
		try {
			Class clase = obj.getClass();
			Object o = clase.newInstance();

			Field[] fields = obj.getClass().getDeclaredFields();

			for (Field f : fields) {
				f.setAccessible(true);
				if (!UtilDB.esAuxiliar(f.getName())) {
					if (f.getType().getSimpleName().equals("String")) {
						try {
							int nc = rs.getColumnIndex(f.getName());
							f.set(o, rs.getString(nc));
						} catch (IllegalArgumentException ex) {
							Logger.getLogger(ManangerSQlite.class.getName())
									.log(Level.SEVERE, null, ex);
						} catch (IllegalAccessException ex) {
							Logger.getLogger(ManangerSQlite.class.getName())
									.log(Level.SEVERE, null, ex);
						}
					} else {
						if (f.getType().getSimpleName().equals("Date")) {
							try {
								int nc = rs.getColumnIndex(f.getName());
								
								Date fecha = UtilDB.FormatoFecha.parse(rs.getString(nc));
								f.set(o, fecha);
							} catch (IllegalArgumentException ex) {
								Logger.getLogger(ManangerSQlite.class.getName())
										.log(Level.SEVERE, null, ex);
							} catch (IllegalAccessException ex) {
								Logger.getLogger(ManangerSQlite.class.getName())
										.log(Level.SEVERE, null, ex);
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							if (f.getType().getSimpleName().equals("Integer")) {
								Integer tipo = new Integer(0);
								try {
									int nc = rs.getColumnIndex(f.getName());
									String nombre = f.getName();
									int valor = rs.getInt(nc);
									f.set(o, valor);
								} catch (IllegalArgumentException ex) {
									Logger.getLogger(
											ManangerSQlite.class.getName())
											.log(Level.SEVERE, null, ex);
								} catch (IllegalAccessException ex) {
									Logger.getLogger(
											ManangerSQlite.class.getName())
											.log(Level.SEVERE, null, ex);
								}
							} else {

								if (f.getType().getSimpleName().equals("Time")) {
									Integer tipo = new Integer(0);
									try {
										int nc = rs.getColumnIndex(f.getName());
										String nombre = f.getName();
										String valor = rs.getString(nc);
										f.set(o, valor);
									} catch (IllegalArgumentException ex) {
										Logger.getLogger(
												ManangerSQlite.class.getName())
												.log(Level.SEVERE, null, ex);
									} catch (IllegalAccessException ex) {
										Logger.getLogger(
												ManangerSQlite.class.getName())
												.log(Level.SEVERE, null, ex);
									}
								} else {
									if (f.getType().getSimpleName()
											.equals("Double")) {
										Double tipo = new Double(0.0);
										try {
											String nombre = f.getName();
											int nc = rs.getColumnIndex(f
													.getName());
											double valor = rs.getDouble(nc);
											f.set(o, valor);
										} catch (IllegalArgumentException ex) {
											Logger.getLogger(
													ManangerSQlite.class
															.getName()).log(
													Level.SEVERE, null, ex);
										} catch (IllegalAccessException ex) {
											Logger.getLogger(
													ManangerSQlite.class
															.getName()).log(
													Level.SEVERE, null, ex);
										}
									}

								}

							}

						}

					}

				}
				f.setAccessible(false);

			}

			return o;
		} catch (InstantiationException ex) {
			Logger.getLogger(ManangerSQlite.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(ManangerSQlite.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		return null;
	}
	public static <T> void imprimir_reves(T objeto) {
        StringBuffer sb = new StringBuffer(objeto.toString());
        System.out.println(sb.reverse());
    }  
    public static <T> void convertir(ArrayList<T> salida,  List<Object> lista) {
        for (int i = 0; i < lista.size(); i++) {
            salida.add((T)lista.get(i));
        }
    }

}
