package com.sya.presupo.estructuras;

import java.util.ArrayList;

import com.sya.presupo.util.MyMath;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.Log;

public class Linea extends figura {
	
	
	public Linea() {
		// TODO Auto-generated constructor stub
		super();
		
	}
	
	
	
	
	public void DibujarLineas(Canvas canvas) {
		// line at minimum...
		if (this.getPuntos().size() < 2) {
			return;
		}
	//	paint.setColor(Color.parseColor());
		ArrayList<Point> p = this.getPuntos();
		int i, len;
		len = p.size();
		Paint paint1 = new Paint();
		paint1.setColor(Color.TRANSPARENT);
		paint1.setAlpha(50);
		paint1.setStrokeWidth(4);
		
		
		for (i = 0; i < len - 1; i++) {
			Point punto = p.get(i);
			Point pf = p.get(i + 1);
			canvas.drawLine(punto.x, punto.y, pf.x, pf.y, paint);

			canvas.drawCircle(punto.x, punto.y, this.getRadio(), paint1);
		}
		Point punto = p.get(len - 1);
		canvas.drawCircle(punto.x, punto.y, this.getRadio(), paint1);

	}
	public int  getIndex(float x, float y){
		
		int n = this.getPuntos().size();
		for (int i = 0; i < n; i++) {
			double ax = x - this.getPuntos().get(i).x;
			double ay = y - this.getPuntos().get(i).y;
			float d = (float) Math.sqrt(ax * ax + ay * ay);
			if (d <= this.getRadio()) {
				this.index = i;
				return this.index;
			}

		}
		return -1;
	}




	@Override
	public void onDibujar(Canvas canvas) {
		// TODO Auto-generated method stub
		DibujarLineas(canvas);
		
	}
	@Override
	public double onCantidad(double pix, double equi) {
		 double aux =  MyMath.getTotalDistanciaLinea(puntos);
		return MyMath.tresSimple(pix, equi, aux);
	}
}
