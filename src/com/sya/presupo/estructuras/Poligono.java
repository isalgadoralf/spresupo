package com.sya.presupo.estructuras;

import java.util.ArrayList;

import com.sya.presupo.util.MyMath;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;

public class Poligono extends figura {
	public ArrayList<Point> ptos = new ArrayList<Point>();
	public Poligono() {
		// TODO Auto-generated constructor stub
		super();
	}

	@Override
	public void onDibujar(Canvas canvas) {
		// TODO Auto-generated method stub
		poli(canvas);
		
	}
	
	public void poli(Canvas canvas) {
		if (this.getPuntos().size() < 2) {
			return;
		}
		Paint paint = new Paint();
		paint.setColor(Color.TRANSPARENT);
		paint.setAlpha(50);
		paint.setStrokeWidth(4);
		paint.setStyle(Paint.Style.FILL);

		DibujarLineas(canvas);
		Path p = PointToPath();
		canvas.drawPath(p, paint);

		canvas.drawPath(path, paint);
	}
	public void DibujarLineas(Canvas canvas) {
		// line at minimum...
		if (this.getPuntos().size() < 2) {
			return;
		}
		ArrayList<Point> p = this.getPuntos();
		int i, len;
		len = p.size();
		Paint paint1 = new Paint();
		paint1.setColor(Color.TRANSPARENT);
		paint1.setAlpha(50);
		paint1.setStrokeWidth(4);

		for (i = 0; i < len - 1; i++) {
			Point punto = p.get(i);
			Point pf = p.get(i + 1);
			canvas.drawLine(punto.x, punto.y, pf.x, pf.y, paint);

			canvas.drawCircle(punto.x, punto.y, this.getRadio(), paint1);
		}
		Point punto = p.get(len - 1);
		canvas.drawCircle(punto.x, punto.y, this.getRadio(), paint1);

	}

	private Path PointToPath() {
		Path path = new Path();
		int n = this.getPuntos().size();
		Point p = this.getPuntos().get(0);
		path.moveTo(p.x, p.y);
		for (int i = 1; i < n; i++) {
			p = this.getPuntos().get(i);
			path.lineTo(p.x, p.y);
		}
		// path.close();
		return path;
	}
public int  getIndex(float x, float y){
		
		int n = this.getPuntos().size();
		for (int i = 0; i < n; i++) {
			double ax = x - this.getPuntos().get(i).x;
			double ay = y - this.getPuntos().get(i).y;
			float d = (float) Math.sqrt(ax * ax + ay * ay);
			if (d <= this.getRadio()) {
				this.index = i;
				return this.index;
			}

		}
		return -1;
	}

	@Override
	public double onCantidad(double pix, double equi) {
		// TODO Auto-generated method stub
		getPuntosArea(pix, equi);
		return MyMath.calculoArea(ptos);
	}
	private  void getPuntosArea(double pix, double equi){
		for (int i = 0; i < puntos.size(); i++) {
			Point aux = puntos.get(i);
			aux = MyMath.cpointTopoint(aux, equi, pix);
			ptos.add(aux);
			
		} 
		 
	}
}
