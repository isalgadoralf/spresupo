package com.sya.presupo.estructuras;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;

public abstract class figura {
	protected int index;
	protected int tipo;
	protected int radio;
	protected ArrayList<Point> puntos;
	protected Paint paint;
	protected Path path;
	
	public figura() {
		// TODO Auto-generated constructor stub
		puntos  = new ArrayList<Point>();
		radio = 50;
		this.paint = new Paint();
		this.paint.setAntiAlias(true);
		this.paint.setDither(true);

		this.paint.setColor(-16711936);
		this.paint.setStyle(Paint.Style.STROKE);
		this.paint.setStrokeJoin(Paint.Join.ROUND);
		this.paint.setStrokeCap(Paint.Cap.ROUND);
		this.paint.setStrokeWidth(9.0F);
		this.path = new Path();
		this.index = -1;
	}
	 public abstract void onDibujar(Canvas canvas);
	 public abstract double onCantidad(double pix,double equi);
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public int getRadio() {
		return radio;
	}
	public void setRadio(int radio) {
		this.radio = radio;
	}
	public ArrayList<Point> getPuntos() {
		return puntos;
	}
	public void setPuntos(ArrayList<Point> puntos) {
		this.puntos = puntos;
	}
	public Path getPath() {
		return path;
	}
	public void setPath(Path path) {
		this.path = path;
	}
	public void setColor(String color) {
	
		this.paint.setColor(Color.parseColor(color));
	}
	
}
