package com.sya.presupo.spresupo;

import java.util.List;

import com.sya.presupo.datos.ManangerSQlite;
import com.sya.presupo.datos.MyDataSource;
import com.sya.presupo.model.Categoria;
import com.sya.presupo.model.Usuario;
import com.sya.presupo.util.UtilDB;

import scz.bo.spresupo.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Data;
import android.telephony.TelephonyManager;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.Toast;

public class FPresupo extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fpresupo);
		//MyDataSource m = new MyDataSource(this);
		UtilDB.cerarDirectorios();
		 Usuario u = new Usuario();
	    ManangerSQlite m =  new ManangerSQlite(this);
	    List a  = m.getLista(u);
	    Puente.registrado = (a.size()>0?true:false);
	   //  Toast.makeText(this, ""+a.size(), Toast.LENGTH_LONG).show();
	   
	}
	

	public void onItems(View v) {
		if(Puente.registrado){
			Intent aux = new Intent(this, FlistaItems.class);
			startActivity(aux);
		}else{
			Intent aux = new Intent(this, FRegistro.class);
			startActivity(aux);
		}
		

	}
	public void onProveedor(View v) {

		Intent aux = new Intent(this, FProvedor.class);
		startActivity(aux);

	}
	public void onPrueba(View v) {
		if(Puente.registrado){
		Intent aux = new Intent(this, FCamara.class);
		startActivity(aux);
		}else{
			Intent aux = new Intent(this, FRegistro.class);
			startActivity(aux);
		}
		

	}
	public void onPresupuesto(View v) {
		if(Puente.registrado){
		Intent aux = new Intent(this, FPresupuestos.class);
		startActivity(aux);
		}else{
			Intent aux = new Intent(this, FRegistro.class);
			startActivity(aux);
		}

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mprincipal, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.actulizarClientes:
			leerContactos();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
		
	
	}
	private void leerContactos(){
		Cursor c = getContentResolver().query(
			     Data.CONTENT_URI,
			     new String[] { Data._ID, Data.DISPLAY_NAME, Phone.NUMBER, Phone.TYPE },
			     Data.MIMETYPE + "='" + Phone.CONTENT_ITEM_TYPE + "' AND "
			     + Phone.NUMBER + " IS NOT NULL", null,
			     Data.DISPLAY_NAME + " ASC");
		String aux = "" ;
		if (c.moveToFirst()) {
			c.moveToNext();
			//do {
			  aux = aux + " , "  +c.getString(0);
			  aux = aux + " , "  +c.getString(1);
			  aux = aux + " , "  +c.getString(2);
		//	} while (c.moveToNext());
		}
		c.close();
		//int aux = c.getCount();
		Toast.makeText(this, ""+aux, Toast.LENGTH_LONG).show();
		
	}
}
