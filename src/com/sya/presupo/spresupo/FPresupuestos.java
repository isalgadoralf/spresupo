package com.sya.presupo.spresupo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.xmlpull.v1.XmlPullParserException;

import com.sya.presupo.adaptadores.ListaPresuAdaptador;
import com.sya.presupo.adaptadores.listaProductosAdaptador;
import com.sya.presupo.datos.ManangerSQlite;
import com.sya.presupo.estructuras.eItems;
import com.sya.presupo.model.Item;
import com.sya.presupo.model.Presupuesto;
import com.sya.presupo.model.Umedida;
import com.sya.presupo.wservices.WSConsultas;


import scz.bo.spresupo.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class FPresupuestos extends Activity {

	private ListView lista;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fpresupuestos);

		lista = (ListView) findViewById(R.id.lvpresupuesto);
		ManangerSQlite m = new ManangerSQlite(this);
		Presupuesto u = new Presupuesto();
		List li = m.getLista(u);
		Log.d("LISTAS", "" + li.size());
		final ArrayAdapter<Object> adapter = new ArrayAdapter<Object>(this,
				android.R.layout.simple_list_item_1, li);

		lista.setAdapter(adapter);
		lista.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Presupuesto p = ((Presupuesto) adapter.getItem(arg2));
				Puente.presupuesto = p;
				
			/*	int id = p.getPresupuestoID();
				Toast.makeText(getApplicationContext(), "" + id,
						Toast.LENGTH_SHORT).show();
				ActualizarTask actualizarTask = new ActualizarTask(p);
				actualizarTask.execute();*/
				onClickVer(arg1);
			}

		});
	}
	
	public void onClickVer(View v){
	//	Puente.lista  = canvas.lsitem;
    	Intent aux = new Intent(this, FVistapresu.class);
		startActivity(aux);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mpresupuesto, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.mipnuevo:
			Intent aux = new Intent(this, Fitems.class);
			startActivity(aux);
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	
}
