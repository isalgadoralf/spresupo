package com.sya.presupo.spresupo;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import scz.bo.spresupo.R;

import com.sya.presupo.adaptadores.listaItemsAdaptador;
import com.sya.presupo.datos.ManangerSQlite;
import com.sya.presupo.estructuras.eItems;
import com.sya.presupo.model.Item;
import com.sya.presupo.model.Proveedores;
import com.sya.presupo.model.Umedida;






import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class FProvedor extends Activity {
	private ListView lista;
	private int nitCliente;
	private ArrayList<Proveedores> listp = new ArrayList<Proveedores>();
	private ArrayList<Proveedores> listaproveedor =  new ArrayList<Proveedores>();
	ArrayAdapter<Proveedores> adapter;
	


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fprovedor);
		lista = (ListView) findViewById(R.id.lvpro);
		cargarLista();
		adapter = new ArrayAdapter<Proveedores>(this,
				android.R.layout.simple_list_item_1, listp);
		lista.setAdapter(adapter);

	}
	private void cargarLista() {
		Proveedores i = new Proveedores();
		ManangerSQlite m = new ManangerSQlite(this);
		List li = m.getLista(i);
		listp = new ArrayList<Proveedores>();
		if (li.size() > 0){
			for (int j = 0; j < li.size(); j++) {
				i =  (Proveedores) li.get(j);
				
				listp.add(i);
				
			}
		}
		
		

	}

	public void actualizar() {
		ManangerSQlite m = new ManangerSQlite(this);
		listp = new ArrayList<Proveedores>();
		for (int j = 0; j < listaproveedor.size(); j++) {
			Proveedores i =  (Proveedores) listaproveedor.get(j);
			
			m.guardar(i);
			
			listp.add(i);
			
		}
	
		
	
		adapter = new ArrayAdapter<Proveedores>(this,
				android.R.layout.simple_list_item_1, listp);
		lista.setAdapter(adapter);

	}

	public void onActualizar(View v) {

		ActualizarTask actualizarTask = new ActualizarTask();
		actualizarTask.execute();

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.iuproveedor, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case R.id.action_settings:
	         ///  Toast.makeText(this, "actualizar", Toast.LENGTH_SHORT).show();
	           
	        	ActualizarTask actualizarTask = new ActualizarTask();
	    		actualizarTask.execute();
	    		
	            return true;
	      
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

	private class ActualizarTask extends AsyncTask<Void, Void, Void> {

		private ProgressDialog mProgressDialog;

		private com.sya.presupo.wservices.WSConsultas mWsBuscador;

		public ActualizarTask() {
			mProgressDialog = new ProgressDialog(FProvedor.this);
			mWsBuscador = new com.sya.presupo.wservices.WSConsultas();
		}

		@Override
		protected void onPreExecute() {

			mProgressDialog.setMessage("Actualizando");
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {

			ArrayList<Object> nuevos;
			try {
				listaproveedor = mWsBuscador.getProveedor("");
				
					ManangerSQlite  m  =  new ManangerSQlite(getApplicationContext());
					Proveedores i = new Proveedores();
					m.eliminarTabla(i);
					//Proveedores p = (Proveedores) listp.get(i);
					
					
			
				Log.d("Preovedor :","ssss: " + listp.size());
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();
			}
			Log.d("WEBSERVICE", "PRODUCTOS: " + listp.size());

			 actualizar();
		}

	}

}
