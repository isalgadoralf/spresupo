package com.sya.presupo.spresupo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import com.sya.presupo.datos.ManangerSQlite;
import com.sya.presupo.model.Presupuesto;
import com.sya.presupo.model.Usuario;

import com.sya.presupo.util.Constantes;
import com.sya.presupo.wservices.WSConsultas;

import scz.bo.spresupo.R;
import scz.bo.spresupo.R.layout;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class FRegistro extends Activity {
	EditText nmobre;
	EditText telefono;// = (EditText) findViewById(R.id.etrtelefono);
	EditText correo;// = (EditText) findViewById(R.id.etrcorreo);
	Usuario usuario;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fregistro);
		nmobre = (EditText) findViewById(R.id.etrnombre);
		telefono = (EditText) findViewById(R.id.etrtelefono);
		correo = (EditText) findViewById(R.id.etrcorreo);
		Button btnAction = (Button) findViewById(R.id.btnregistrar);
		btnAction.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				guardar();

			}
		});
	}

	public void guardar() {
		EditText nmobre = (EditText) findViewById(R.id.etrnombre);
		EditText telefono = (EditText) findViewById(R.id.etrtelefono);
		EditText correo = (EditText) findViewById(R.id.etrcorreo);
		TelephonyManager mngr = (TelephonyManager) this
				.getSystemService(this.TELEPHONY_SERVICE);
		String imei = mngr.getDeviceId();
		usuario = new Usuario(correo.getText().toString(), imei, nmobre
				.getText().toString(), "", telefono.getText().toString(),
				hashCode());
		ManangerSQlite m = new ManangerSQlite(this);
		ActualizarTask actualizarTask = new ActualizarTask(Constantes.VERIFICAR);
		actualizarTask.execute();
		if (Puente.conexion) {
			boolean sw = m.guardarID(usuario);

			
			if (sw) {
				ActualizarTask actualizarTask2 = new ActualizarTask(Constantes.GUARDAR);
				actualizarTask2.execute();
				Puente.registrado = sw;

			}
		}else{
			Puente.registrado =  false;
			Toast.makeText(this, "NO HAY CONEXION VULEVA INTENTAR MAS TARDE", Toast.LENGTH_SHORT).show();
		}

	}

	public int hashCode() {
		int result = 17;
		result = 37 * result + nmobre.hashCode();
		result = 37 * result + telefono.hashCode();
		result = 37 * result + correo.hashCode();
		Date fecha = new Date();
		result = 37 * result + fecha.hashCode();
		return result;
	}

	private class ActualizarTask extends AsyncTask<Void, Void, Void> {

		private ProgressDialog mProgressDialog;
		private int modo = -1;

		private WSConsultas mWsBuscador;

		public ActualizarTask(int modo) {
			mProgressDialog = new ProgressDialog(FRegistro.this);
			mWsBuscador = new WSConsultas();
			this.modo = modo;

		}

		@Override
		protected void onPreExecute() {

			mProgressDialog.setMessage("Registrando..");
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {

			String aux = "";

			try {
				if (modo == Constantes.GUARDAR) {
					aux = mWsBuscador.guardarUsuario(usuario);
				} else {
					Puente.conexion = mWsBuscador.verificarConexion();
				}

				if (aux.length() > 0) {
					Log.d("WEBSERFPRESU", " SE REGISTRO CORRECTAMENTE ");
				} else {
					Log.d("WEBSERFPRESU", "NO SE REGISTRO ");
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		/*
		 * private ArrayList<Productos> wsToDatos(ArrayList<wservices.Productos>
		 * lista){ ArrayList<Productos> aux = new ArrayList<Productos>(); for
		 * (int i = 0; i < lista.size(); i++) { aux.add(parse(lista.get(i))); }
		 * return aux; } private Datos.Productos parse(wservices.Productos p){
		 * Datos.Productos aux = new
		 * Productos(p.getId(),p.getDescripcion(),p.getIdcategoria
		 * (),p.getFechaactualizacion(),p.getPrecio(),null); return aux;
		 * 
		 * }
		 */
		@Override
		protected void onPostExecute(Void result) {

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();
			}

		}

	}

}
