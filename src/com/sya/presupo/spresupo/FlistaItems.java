package com.sya.presupo.spresupo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import scz.bo.spresupo.R;

import com.sya.presupo.adaptadores.listaItemsAdaptador;
import com.sya.presupo.adaptadores.listaProductosAdaptador;
import com.sya.presupo.datos.ManangerSQlite;
import com.sya.presupo.estructuras.eItems;
import com.sya.presupo.model.Item;
import com.sya.presupo.model.Umedida;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class FlistaItems extends Activity {
	private static final String TAG = FlistaItems.class.getSimpleName();
	private ListView lista;
	// private NProducto productos;
	private int nitCliente;
	private ArrayList<eItems> listp;
	private ArrayList<Item> listaItems =  new ArrayList<Item>();

	private listaItemsAdaptador lpa;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_flista_items);
		

		lista = (ListView) findViewById(R.id.lvlistaitems);

		cargarLista();

		lpa = new listaItemsAdaptador(this, listp);
		// ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
		// android.R.layout.simple_list_item_1, android.R.id.text1,
		// productos.getTodos());
		lista.setAdapter(lpa);
		lista.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub

				Toast.makeText(
						getApplicationContext(),
						((listaProductosAdaptador) arg0.getAdapter()).list.get(
								arg2).getDescripcion(), Toast.LENGTH_SHORT)
						.show();

				if (((listaProductosAdaptador) arg0.getAdapter()).list
						.get(arg2).isChecable()) {
					((listaProductosAdaptador) arg0.getAdapter()).list
							.get(arg2).setChecable(false);
				} else {
					// aqui al contrario que la anterior, que lo pase a true.
					((listaProductosAdaptador) arg0.getAdapter()).list
							.get(arg2).setChecable(true);

					// IMPORTANTE
					// ((listaProductosAdaptador)arg0.getAdapter()).list.get(arg2).setDescripcion("HOLA MUDNO");
					// ((listaProductosAdaptador)arg0.getAdapter()).list.remove(arg2);

				}
				// Se notifica al adaptador de que el ArrayList que tiene
				// asociado ha sufrido cambios (forzando asi a ir al metodo
				// getView())
				((listaProductosAdaptador) arg0.getAdapter())
						.notifyDataSetChanged();
			}

		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mlistaitems, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case R.id.mliactualizar:
	        	ActualizarTask actualizarTask = new ActualizarTask();
	    		actualizarTask.execute();
	            return true;
	      
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	private void cargarLista() {
		Item i = new Item();
		ManangerSQlite m = new ManangerSQlite(this);
		List li = m.getLista(i);
		listp = new ArrayList<eItems>();
		if (li.size() > 0){
			for (int j = 0; j < li.size(); j++) {
				i =  (Item) li.get(j);
				Umedida u = new Umedida();
				u.setUmedidaID(i.getUmedidaID());
				u =  (Umedida) m.getObjectId(u);
				eItems e = new eItems(i.getItemID(),i.getDescripcion(), i.getPrecio(), u.getAbreviatura());
				listp.add(e);
				
			}
		}
		
		

	}
	private class ActualizarTask extends AsyncTask<Void, Void, Void> {

		private ProgressDialog mProgressDialog;

		private com.sya.presupo.wservices.WSConsultas mWsBuscador;

		public ActualizarTask() {
			mProgressDialog = new ProgressDialog(FlistaItems.this);
			mWsBuscador = new com.sya.presupo.wservices.WSConsultas();
		}

		@Override
		protected void onPreExecute() {

			mProgressDialog.setMessage("Actualizando");
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {

			
			try {
				listaItems = mWsBuscador.getItems("");
				if(listaItems.size()>0){
					ManangerSQlite  m  =  new ManangerSQlite(getApplicationContext());
					Item i = new Item();
					m.eliminarTabla(i);
					
				}
				Log.d("WEBSERVICE", "ssss: " + listaItems.size());
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			if (mProgressDialog != null) {
				mProgressDialog.dismiss();
			}
			Log.d("WEBSERVICE", "PRODUCTOS: " + listaItems.size());

			 actualizar();
		}

	}
	public void actualizar() {
		ManangerSQlite m = new ManangerSQlite(this);
		listp = new ArrayList<eItems>();
		for (int j = 0; j < listaItems.size(); j++) {
			Item i =  (Item) listaItems.get(j);
			Umedida u = new Umedida();
			m.guardar(i);
			u.setUmedidaID(i.getUmedidaID());
			u =  (Umedida) m.getObjectCampo(u, "umedidaID", i.getUmedidaID());
			eItems e = new eItems(i.getItemID(),i.getDescripcion(), i.getPrecio(), u.getAbreviatura());
			listp.add(e);
			
		}
		lpa = new listaItemsAdaptador(this, listp);
		lista.setAdapter(lpa); 

	}
}
