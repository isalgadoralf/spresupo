package com.sya.presupo.spresupo;

import java.util.ArrayList;

import com.sya.presupo.datos.ManangerSQlite;
import com.sya.presupo.estructuras.Linea;
import com.sya.presupo.estructuras.Point;
import com.sya.presupo.estructuras.Poligono;
import com.sya.presupo.estructuras.eItems;
import com.sya.presupo.estructuras.figura;
import com.sya.presupo.model.Item;
import com.sya.presupo.model.Umedida;
import com.sya.presupo.util.Constantes;
import com.sya.presupo.util.MyMath;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

@SuppressLint("ClickableViewAccessibility")
public class MyCanvas extends View {
	// private Canvas canvas;
	final Paint paint;
	private Bitmap bitmap;
	public double equi;
	public double pix;
	public double altura;
	public eItems it;
	public ArrayList<Point> puntos = new ArrayList<Point>();
	public ArrayList<eItems> lsitem = new ArrayList<eItems>();
	public boolean isarea;
	public Path path;

	public int accion;
	public int index;

	private Linea lnorigin;
	public int lnindex;
	public int tipoGrafica;

	public ArrayList<Linea> listaLineas = new ArrayList<Linea>();
	public ArrayList<Poligono> listaPoli = new ArrayList<Poligono>();
	// public Point indexLista = new Point(-1,-1);
	public int lineaindex;
	public String color;
	private Linea lineas;
	private Poligono plineas;

	public int indexlista = -1;
	public int indexlistaSub = -1;

	public MyCanvas(Context context, int w, int h) {
		super(context);
		// TODO Auto-generated constructor stub
		bitmap = Constantes.resizeBitmapAnchoAlto(Puente.imagen, h, w);
		lineas = new Linea();
		plineas = new Poligono();
		accion = 0;
		index = -1;
	
		this.paint = new Paint();
		this.paint.setAntiAlias(true);
		this.paint.setDither(true);

		this.paint.setColor(-16711936);
		this.paint.setStyle(Paint.Style.STROKE);
		this.paint.setStrokeJoin(Paint.Join.ROUND);
		this.paint.setStrokeCap(Paint.Cap.ROUND);
		this.paint.setStrokeWidth(9.0F);
		this.path = new Path();
		this.isarea = false;
		// init();
		lnorigin = new Linea();
		lineaindex = -1;
		tipoGrafica = Constantes.ORIGEN;
	}

	public void destroy() {
		if (bitmap != null) {
			bitmap.recycle();
		}
	}

	public void agregarNuevoLinea() {
		lineas = new Linea();
		listaLineas.add(lineas);
		lineaindex++;
		lineas = listaLineas.get(lineaindex);
	}
	
	public void nuvevoPoli(){
		indexlista = -1;
		indexlistaSub = -1;
		lineaindex = -1;
		listaPoli = new ArrayList<Poligono>();
		agregarNuevoPoli();
	}
	public void nuvevoLinea(){
		indexlista = -1;
		indexlistaSub = -1;
		lineaindex = -1;
		listaLineas = new ArrayList<Linea>();
		agregarNuevoLinea();
	}
	

	public void agregarNuevoPoli() {
		plineas = new Poligono();
		listaPoli.add(plineas);
		lineaindex++;
		plineas = listaPoli.get(lineaindex);

	}

	@Override
	public void onDraw(Canvas c) {

		c.drawBitmap(bitmap,
				new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()),
				new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()), null);

		if (it != null) {
			// AREA EN EL PISO
			if (it.getTipo() == 2) {
				// poli(c);
				for (int i = 0; i < listaPoli.size(); i++) {
					listaPoli.get(i).setColor(color);
					listaPoli.get(i).poli(c);
				}
			}

			// DIBUAJAR LINEAS
			if (it.getTipo() == 0) {
				// drawPoly(c, puntos);
				// DibujarLineas(c, lineas);
				for (int i = 0; i < listaLineas.size(); i++) {
					listaLineas.get(i).setColor(color);
					listaLineas.get(i).DibujarLineas(c);
				}
			}
			if (it.getTipo() == 1) {
				// drawPoly(c, puntos);
				DibujarPuntos(c);
			}
		} else {// originen
			lnorigin.DibujarLineas(c);
			// drawPoly(c);
		}

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		float x = event.getX();
		float y = event.getY();

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:

			this.accionPresionar(x, y);
			break;
		case MotionEvent.ACTION_MOVE:
			Log.e("MOVE", "" + x + "  " + y + " " + this.accion + "  "
					+ tipoGrafica);
			if (accion == 1) {
				switch (tipoGrafica) {
				case 3:
					if (lnindex > -1) {
						lnorigin.getPuntos().get(lnindex).x = x;
						lnorigin.getPuntos().get(lnindex).y = y;

						invalidate();
					}
					break;
				case 0:
					if (it.getTipo() == 0)
						if (indexlista > -1) {
							listaLineas.get(indexlista).getPuntos()
									.get(indexlistaSub).x = x;
							listaLineas.get(indexlista).getPuntos()
									.get(indexlistaSub).y = y;

							invalidate();
						}
					if (it.getTipo() == 2)
						if (indexlista > -1) {
							listaPoli.get(indexlista).getPuntos()
									.get(indexlistaSub).x = x;
							listaPoli.get(indexlista).getPuntos()
									.get(indexlistaSub).y = y;

							invalidate();
						}
					if (it.getTipo() == 1)
						if (indexlista > -1) {
							puntos.get(indexlista).x = x;
							puntos.get(indexlista).y = y;

							invalidate();
						}
					break;

				default:
					break;
				}

			}

			break;

		}
		return true;
	}

	private void drawPoly(Canvas canvas) {
		// line at minimum...
		if (lineas.getPuntos().size() < 2) {
			return;
		}

		ArrayList<Point> p = lineas.getPuntos();
		int i, len;
		len = p.size();
		Paint paint1 = new Paint();
		paint1.setColor(Color.TRANSPARENT);
		paint1.setAlpha(50);
		paint1.setStrokeWidth(4);
		for (i = 0; i < len - 1; i++) {
			Point punto = p.get(i);
			Point pf = p.get(i + 1);
			canvas.drawLine(punto.x, punto.y, pf.x, pf.y, paint);

			canvas.drawCircle(punto.x, punto.y, this.lineas.getRadio(), paint1);
		}
		Point punto = p.get(len - 1);
		canvas.drawCircle(punto.x, punto.y, lineas.getRadio(), paint1);

	}

	public void calexquivalencia() {
		Point a = lnorigin.getPuntos().get(0);
		Point b = lnorigin.getPuntos().get(1);
		this.pix = MyMath.distancia(a, b);

	}

	public void clearCanvas() {
		this.puntos = new ArrayList<Point>();
		lineas = new Linea();
		invalidate();
	}

	public double calcularMuro() {
		double cantidad = MyMath.getTotalDistancia(lineas.getPuntos(), equi,
				pix);
		it.setCantidad(MyMath.Redondear(cantidad));
		return cantidad;
	}

	public void calcularLinea() {
		double aux = 0;
		for (int i = 0; i < listaLineas.size(); i++) {
			aux = aux + listaLineas.get(i).onCantidad(pix, equi);
		}
		aux = MyMath.Redondear(aux);
		it.setCantidad(aux);
		// return aux;
	}

	public void calcularPoli() {
		double aux = 0;
		for (int i = 0; i < listaPoli.size(); i++) {
			aux = aux + listaPoli.get(i).onCantidad(pix, equi);
		}
		aux = MyMath.Redondear(aux);
		if (aux < 0)
			aux = aux * -1;
		it.setCantidad(aux);
		// return aux;
	}

	public void poli(Canvas canvas) {
		if (plineas.getPuntos().size() < 2) {
			return;
		}
		Paint paint = new Paint();
		paint.setColor(Color.TRANSPARENT);
		paint.setAlpha(50);
		paint.setStrokeWidth(4);
		paint.setStyle(Paint.Style.FILL);

		DibujarLineas(canvas, plineas);
		Path p = PointToPath();
		canvas.drawPath(p, paint);

		canvas.drawPath(path, paint);
	}

	private Path PointToPath() {
		Path path = new Path();
		int n = plineas.getPuntos().size();
		Point p = plineas.getPuntos().get(0);
		path.moveTo(p.x, p.y);
		for (int i = 1; i < n; i++) {
			p = plineas.getPuntos().get(i);
			path.lineTo(p.x, p.y);
		}
		// path.close();
		return path;
	}

	public void Nuevo() {

		this.clearCanvas();
		this.lsitem.clear();
		this.path = new Path();
		this.isarea = false;
		this.puntos = new ArrayList<Point>();
		this.accion = 0;

		lineas = new Linea();
	}
	public void crearNuvosPuntos() {
		this.puntos = new ArrayList<Point>();
	}

	public void DibujarLineas(Canvas canvas, figura puntos) {
		// line at minimum...
		if (puntos.getPuntos().size() < 2) {
			return;
		}
		ArrayList<Point> p = puntos.getPuntos();
		int i, len;
		len = p.size();
		Paint paint1 = new Paint();
		paint1.setColor(Color.TRANSPARENT);
		paint1.setAlpha(50);
		paint1.setStrokeWidth(4);

		for (i = 0; i < len - 1; i++) {
			Point punto = p.get(i);
			Point pf = p.get(i + 1);
			canvas.drawLine(punto.x, punto.y, pf.x, pf.y, paint);

			canvas.drawCircle(punto.x, punto.y, puntos.getRadio(), paint1);
		}
		Point punto = p.get(len - 1);
		canvas.drawCircle(punto.x, punto.y, puntos.getRadio(), paint1);

	}

	public void DibujarPuntos(Canvas canvas) {
		// line at minimum...
		if (puntos.size() < 1) {
			return;
		}
		ArrayList<Point> p = puntos;
		int i, len;
		len = p.size();
		Paint paint1 = new Paint();
		paint1.setColor(Color.TRANSPARENT);
		paint1.setAlpha(50);
		paint1.setStrokeWidth(4);

		for (i = 0; i < len; i++) {
			Point punto = p.get(i);
			// Point pf = p.get(i);
			// canvas.drawLine(punto.x, punto.y, pf.x, pf.y, paint);

			canvas.drawCircle(punto.x, punto.y, 50, paint1);
		}

	}

	public void accionPresionar(float x, float y) {
		if (it == null) {
			// puntos.add(new Point(x, y));
			if (this.accion == 0) {
				lnorigin.getPuntos().add(new Point(x, y));
				invalidate();
			}
			if (this.accion == 1) {
				this.lnindex = lnorigin.getIndex(x, y);
				invalidate();
			}
		} else {
			Log.e("DONW",
					"" + x + "  " + y + " " + this.accion + "  " + it.getTipo());
			if (this.accion == 0) { // //////// NUEVO
				switch (it.getTipo()) {
				case 0: // LINEA
					puntos.add(new Point(x, y));
					lineas.getPuntos().add(new Point(x, y));
					Log.e("PUNTOS", "" + lineas.getPuntos().size());
					invalidate();
					break;
				case 1:// PUNTO
					puntos.add(new Point(x, y));
					// lineas.getPuntos().add(new Point(x, y));
					Log.e("PUNTOS", "" + puntos.size());
					invalidate();
					break;

				case 2:// AREA

					plineas.getPuntos().add(new Point(x, y));
					Log.e("PUNTOsssS", "" + plineas.getPuntos().size());
					
					invalidate();
					break;

				}
			} else {
				if (this.accion == 1) { // //////////// MODIFICAR
					if (it.getTipo() == 0)
						this.buscarLinea(x, y);
					if (it.getTipo() == 2)
						this.buscarPoli(x, y);
					if (it.getTipo() == 1)
						this.getIndex(x, y);
					Log.e("INDEXLISTA", "" + indexlista);
				}
			}

		}

	}

	public void buscarLinea(float x, float y) {

		int j = -1;
		for (int i = 0; i < listaLineas.size(); i++) {
			j = listaLineas.get(i).getIndex(x, y);
			if (j > 0) {
				indexlista = i;
				indexlistaSub = j;
				return;
			}
		}
		indexlistaSub = -1;
		indexlista = -1;

	}

	public void buscarPoli(float x, float y) {

		int j = -1;
		for (int i = 0; i < listaPoli.size(); i++) {
			j = listaPoli.get(i).getIndex(x, y);
			if (j > 0) {
				indexlista = i;
				indexlistaSub = j;
				return;
			}
		}
		indexlistaSub = -1;
		indexlista = -1;

	}

	public void getIndex(float x, float y) {

		int n = puntos.size();
		for (int i = 0; i < n; i++) {
			double ax = x - puntos.get(i).x;
			double ay = y - puntos.get(i).y;
			float d = (float) Math.sqrt(ax * ax + ay * ay);
			if (d <= 70) {
				this.indexlista = i;
				return;
			}

		}
		indexlista = -1;
		
	}

	public void setAccion(int accion) {
		this.accion = accion;
	}

	public int getAccion() {
		return accion;
	}
}
