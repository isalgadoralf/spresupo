package com.sya.presupo.wservices;

import java.util.Collection;

public class Cliente {
	   private int nit;
	   private String nombre;
	   private String direccion;
	   private String telefono;
	   
	   public Collection<Cliente> lista;
	   public Cliente() {
	   }

	   
	   public Cliente(int nit, String nombre, String direccion, String telefon) {
	       this.nit = nit;
	       this.nombre = nombre;
	       this.direccion = direccion;
	       this.telefono = telefon;
	   }

	   /**
	    * @return the nit
	    */
	   public int getNit() {
	       return nit;
	   }

	   /**
	    * @param nit the nit to set
	    */
	   public void setNit(int nit) {
	       this.nit = nit;
	   }

	   /**
	    * @return the nombre
	    */
	   public String getNombre() {
	       return nombre;
	   }

	   /**
	    * @param nombre the nombre to set
	    */
	   public void setNombre(String nombre) {
	       this.nombre = nombre;
	   }

	   /**
	    * @return the direccion
	    */
	   public String getDireccion() {
	       return direccion;
	   }

	   /**
	    * @param direccion the direccion to set
	    */
	   public void setDireccion(String direccion) {
	       this.direccion = direccion;
	   }

	   /**
	    * @return the telefon
	    */
	   public String getTelefon() {
	       return telefono;
	   }

	   /**
	    * @param telefon the telefon to set
	    */
	   public void setTelefono(String telefon) {
	       this.telefono = telefon;
	   }
	   
	       
	}
