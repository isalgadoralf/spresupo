package com.sya.presupo.wservices;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Rafael
 */
public class Pedido {
    private int id;
    private Date fecha;
    private double longitud;
    private double latitud;
    private String observacion;
    private int nit;
    private int estado;
    private ArrayList<Detalle> detalle;

    public Pedido(int id, Date fecha, double longitud, double latitud, String observacion, int nit, int estado, ArrayList<Detalle> detalle) {
        this.id = id;
        this.fecha = fecha;
        this.longitud = longitud;
        this.latitud = latitud;
        this.observacion = observacion;
        this.nit = nit;
        this.estado = estado;
        this.detalle = detalle;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the longitud
     */
    public double getLongitud() {
        return longitud;
    }

    /**
     * @param longitud the longitud to set
     */
    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    /**
     * @return the latitud
     */
    public double getLatitud() {
        return latitud;
    }

    /**
     * @param latitud the latitud to set
     */
    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    /**
     * @return the observacion
     */
    public String getObservacion() {
        return observacion;
    }

    /**
     * @param observacion the observacion to set
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    /**
     * @return the nit
     */
    public int getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(int nit) {
        this.nit = nit;
    }

    /**
     * @return the estado
     */
    public int getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(int estado) {
        this.estado = estado;
    }

    /**
     * @return the detalle
     */
    public ArrayList<Detalle> getDetalle() {
        return detalle;
    }

    /**
     * @param detalle the detalle to set
     */
    public void setDetalle(ArrayList<Detalle> detalle) {
        this.detalle = detalle;
    }
    
    
}
