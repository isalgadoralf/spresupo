package com.sya.presupo.wservices;

import java.util.Date;

/**
 *
 * @author Rafael
 */
public class Productos {
    private int id;
    private String descripcion;
    private double precio;
    private Date fechaactualizacion;
    private int idcategoria;

    public Productos(int id, String descripcion, double precio, Date fechaactualizacion, int idcategoria) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
        this.fechaactualizacion = fechaactualizacion;
        this.idcategoria = idcategoria;
    }

    public Productos() {
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the precio
     */
    public double getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    /**
     * @return the fechaactualizacion
     */
    public Date getFechaactualizacion() {
        return fechaactualizacion;
    }

    /**
     * @param fechaactualizacion the fechaactualizacion to set
     */
    public void setFechaactualizacion(Date fechaactualizacion) {
        this.fechaactualizacion = fechaactualizacion;
    }

    /**
     * @return the idcategoria
     */
    public int getIdcategoria() {
        return idcategoria;
    }

    /**
     * @param idcategoria the idcategoria to set
     */
    public void setIdcategoria(int idcategoria) {
        this.idcategoria = idcategoria;
    }
    
            
}
