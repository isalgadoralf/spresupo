package com.sya.presupo.wservices;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import android.util.Log;

/**
 * 
 * @author Jorge
 * 
 */
public class SocketClient {

	private static final String TAG = SocketClient.class.getSimpleName();

	private String serverMessage;
	public static final String SERVERIP = "192.168.1.101";
	public static final int SERVERPORT = 2050;
	private OnMessageReceived mMessageListener = null;
	private boolean mRun = true;
	private DataInputStream mDataInputStream;
	private DataOutputStream mDataOutputStream;
	private Socket mSocket;

	
	private int ID;
	private boolean sw;
	/**
	 * Constructor of the class. OnMessagedReceived listens for the messages
	 * received from server
	 */
	public SocketClient(OnMessageReceived listener) {
		ID = -1;
		sw=false;
		try {
			mSocket = new Socket(SERVERIP, SERVERPORT);
			mDataOutputStream = new DataOutputStream(mSocket.getOutputStream());
			mDataInputStream = new DataInputStream(mSocket.getInputStream());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mMessageListener = listener;
	}
	
	public void setID(int ID){
		this.ID =ID;
		
	}
	
	public int getID(){
		return this.ID;
	}

	public void registrar(){
		if(!sw )
			if(ID!=-1){
				String nick=""+ID;
				String datagram = "SERVER&LOGUEAR&"+nick;
				sendMessage(datagram);
				sw=true;
			}
		
	}
	
	public void sendMessage(String message) {
		if (mDataOutputStream != null) {
			try {
				
				Log.e(TAG, "" + String.valueOf(mSocket.getLocalAddress()));
				mDataOutputStream.writeUTF(message);
				mDataOutputStream.flush();
				
			} catch (IOException e) {
				Log.e(TAG, "" + e.getMessage());
			}

		}
	}

	public void stopClient() {
		mRun = false;
		ID=-1;
		sw =false;
		if (mSocket != null) {
			try {
				mSocket.close();
			} catch (IOException e) {
				Log.e(TAG, "" + e.getMessage());
			}
		}
	}

	public void run() {

		mRun = true;

		try {

			//mSocket = new Socket(SERVERIP, SERVERPORT);

			try {

				//mDataOutputStream = new DataOutputStream(mSocket.getOutputStream());
			//	mDataInputStream = new DataInputStream(mSocket.getInputStream());

				while (mRun) {
					serverMessage = mDataInputStream.readUTF();

					if (serverMessage != null && mMessageListener != null) {
						mMessageListener.messageReceived(serverMessage);
					}
					serverMessage = null;

				}

			} catch (Exception e) {

				Log.e(TAG, "" + e.getMessage());

			} finally {
				mSocket.close();
			}

		} catch (Exception e) {

			Log.e(TAG, "" + e.getMessage());

		}

	}

	public interface OnMessageReceived {
		public void messageReceived(String message);
	}
}
