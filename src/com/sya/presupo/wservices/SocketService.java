package com.sya.presupo.wservices;

import com.sya.presupo.spresupo.FPrincipal;
import com.sya.presupo.wservices.SocketClient.OnMessageReceived;




import android.R;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;



/**
 * 
 * @author Jorge
 * 
 */
public class SocketService extends Service {

	private static final int NOTIFICATION_ID = 2084;
	private SocketClient mSocketClient;
	private final IBinder binder = new SocketBinder();

	private int idCliente;
	
	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}

	public class SocketBinder extends Binder {

		public SocketService getService() {
			return SocketService.this;
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		new ObtenerDatosTask().execute();
		idCliente = intent.getExtras().getInt("id");
		return (START_STICKY);
	}

	private class ObtenerDatosTask extends AsyncTask<Void, String, Void> {

		@Override
		protected Void doInBackground(Void... params) {

			mSocketClient = new SocketClient(new OnMessageReceived() {

				@Override
				public void messageReceived(String message) {

					publishProgress(message);

				}
			});
			mSocketClient.setID(idCliente);
			mSocketClient.registrar();
			mSocketClient.run();

			return null;
		}

		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);

			Log.i("TAG", "Mensaje: " + values[0]);

			// ## Mostramos una notificacion
			NotificationCompat.Builder builder = new NotificationCompat.Builder(
					SocketService.this);

			builder.setSmallIcon(R.drawable.btn_star);
			builder.setTicker("Nueva Actualizacion");
			builder.setWhen(System.currentTimeMillis());
			builder.setContentTitle("Actualizacion");
			builder.setContentText(values[0]);

			 Intent notificationIntent = new Intent(SocketService.this,
			 FPrincipal.class);
			 notificationIntent.putExtra("mensaje", values[0]);
			/*Intent notificationIntent = new Intent(
					android.content.Intent.ACTION_VIEW,
					Uri.parse("http://www.whatsapp.com/android/current/WhatsApp.apk"));*/

			PendingIntent contentIntent = PendingIntent.getActivity(
					SocketService.this, 0, notificationIntent,
					PendingIntent.FLAG_CANCEL_CURRENT);

			builder.setDefaults(Notification.DEFAULT_ALL);
			builder.setContentIntent(contentIntent);
			builder.setAutoCancel(true);

			NotificationManager notificationManager = (NotificationManager) SocketService.this
					.getSystemService(Context.NOTIFICATION_SERVICE);
			notificationManager.notify(NOTIFICATION_ID, builder.build());

		}

	}

	public void sendMessage(String message) {
		if (mSocketClient != null) {
			mSocketClient.sendMessage(message);
		
		}
	}

	public void stopClient() {
		if (mSocketClient != null) {
			mSocketClient.stopClient();
		}
	}

}
